package tfs.scala.http.client

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{HttpRequest, Uri}
import akka.stream.ActorMaterializer
import akka.testkit.TestKit
import tfs.scala.http.client.util.{HttpClientTest, Measure}

import scala.concurrent.Future

class AsyncHttpClientTest extends TestKit(ActorSystem()) with HttpClientTest {
  implicit val materializer = ActorMaterializer()

  behavior of "AsyncHttpClient"

  it should s"send $iteration async requests" in Measure {
    Future.traverse((1 to iteration).toList) { _ =>
      Http()
        .singleRequest(HttpRequest(uri = Uri(s"https://httpbin.org/delay/${timeout.toSeconds}")))
        .map(_.discardEntityBytes())
    }.futureValue
  }
}